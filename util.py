import cv2
import os
import json
import numpy as np
import sys

# режимы, поддерживаемые камерой Rithmix (USB PHY 2.0: USB CAMERA)
STD_RESOLUTIONS = {'1920x1080': (1920, 1080), '1280x720': (1280, 720), '640x480': (640, 480), '640x320': (640, 320)}

class NumpyArrayEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def list_ports():
    available_ports = []
    for dev_port in range(5):
        cap = cv2.VideoCapture(dev_port)
        ret, frame = cap.read()
        if ret:
            available_ports.append(dev_port)
            cap.release()
    return available_ports


def get_camera_info(index):
    import platform
    name = None
    if platform.system() == "Linux":
        device_name = os.path.relpath("/sys/class/video4linux/video" + str(index) + "/name")
        if os.path.isfile(device_name):
            with open(device_name, "r") as name_file:
                name = name_file.read().rstrip()
    else:
        raise NotImplementedError("Not implemented for " + platform.system())
    return name


def get_camera_id_from_user(msg='Please select camera', exclude_devs=[]):
    number = None
    print(msg)
    for dev_idx in list_ports():
        if not dev_idx in exclude_devs:
            print('[' + str(dev_idx) + '] - ', get_camera_info(dev_idx))
    nb = input('Choose a number: ')
    try:
        number = int(nb)
    except ValueError:
        print("Invalid number")
    return number


def get_resolution_from_user():
    resolution = None
    modes = {}
    i = 0
    print('Please select resolution')
    for key, _ in STD_RESOLUTIONS.items():
        modes[i] = key
        print('[' + str(i) + ']: ' + key)
        i += 1
    nb = input('Choose a number: ')
    try:
        number = int(nb)
        resolution = modes[number]
    except (ValueError, IndexError):
        print("Invalid number")
    return resolution

def save_cam_params(params, camera_name, resolution):
    path = 'config'
    if not os.path.exists(path):
        os.makedirs(path)

    file_name = os.path.join(path, camera_name+'_'+resolution+'.json')
    with open(file_name,'w') as config_file:
        json_object = json.dumps(params, indent=4, cls=NumpyArrayEncoder)
        config_file.write(json_object)


def load_cam_params(camera_name, resolution):
    file_name = os.path.join('config', camera_name+'_'+resolution+'.json')
    with open(file_name, 'r') as config_file:
        json_object = json.load(config_file)
        mtx = np.asarray(json_object['mtx'])
        dist = np.asarray(json_object['dist'])
    return mtx, dist


def set_camera_resolution(cap, resolution):
    try:
        width, height = STD_RESOLUTIONS[resolution]
    except KeyError:
        print(resolution + 'is not found in STD_RESOLUTIONS')
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    return cap


